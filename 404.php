<?php header("HTTP/1.1 404 Not Found"); ?>
<?php header("Status: 404 Not Found"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137548613-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-137548613-1');
    </script>

    <link rel="icon" type="image/png" href="images/favicon.png">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Mercury Web Solutions | Web Design Melbourne</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:400,400i,500,600,700" rel="stylesheet" />
    <link rel="stylesheet" href="css/initial.css" />
    <link rel="stylesheet" href="css/theme/style.css" />
    <link rel="stylesheet" href="css/theme/404.css" />
    <link rel="stylesheet" href="css/theme/fonts.css" />
    <link rel="stylesheet" href="css/theme/sections/footer.css" />
</head>

<body>
    <main>
        <img id="logo" src="images\logo.png" alt="Mercury Web Solutions">
        <p id="oops">404</p>
        <p id="text">The page you're looking for could not be found</p>
        <a id="home" href="/">Back to home page</a>
    </main>
    <footer id="footer">
        <div class="footer-seperator"></div>
        <p><a href="docs/Privacy_Policy.pdf" target="_blank">Privacy Policy</a><br>Copyright &copy; Mercury Web Solutions 2019</p>
    </footer>
</body>
</html>