; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.carousel", {
        // Default options
        options: {
            multiCol: false,
            multiRow: false,
            itemWidth: 150,
            itemHeight: 150,
            growItemWidth: false,
            growItemHeight: false,
            itemMarginLeft: 30,
            itemMarginTop: 30,
            growItemMarginLeft: false,
            growItemMarginTop: false,
            pagination: false
        },

        // Constructor
        _create: function () {
            this.carousel = this.element;
            this.list = this.carousel.find('.carousel-list');
            this.items = null;
            this.prevArrow = this.carousel.find('.carousel-arrow.prev');
            this.nextArrow = this.carousel.find('.carousel-arrow.next');
            this.pagination = null;
            this.nav = this.carousel.find('.carousel-nav');
            this.progress = this.carousel.find('.carousel-progress');
            this.indicators = null;
            this.nColumns = 0, this.nRows = 0;
            this.itemWidth = 0, this.itemHeight = 0;
            this.itemMarginLeft = 0, this.itemMarginTop = 0;
            this.listWidth = 0, this.pageWidth = 0;
            this.nPages = 0, this.currentPageIndex = 0;

            // Allow touch events
            this.carousel.hammer();

            // Draw the carousel
            this._drawCarousel();
            // Set up the arrows and indicators
            this._checkControls();

            /*** Bind events ***/
            // On window event
            this._on($(window), {
                // Resize
                resize: function () {
                    // Redraw carousel
                    this.redraw(true);
                }
            });
            // On carousel event
            this._on(this.carousel, {
                // Swipe left
                swipeleft: function () {
                    // Go to next page
                    this._goToPage(this.currentPageIndex + 1);
                },
                // Swipe right
                swiperight: function () {
                    // Go to previous page
                    this._goToPage(this.currentPageIndex - 1);
                }
            });
            // On prevArrow event
            this._on(this.prevArrow, {
                // Click
                click: function () {
                    // Go to previous page
                    this._goToPage(this.currentPageIndex - 1);
                }
            });
            // On nextArrow event
            this._on(this.nextArrow, {
                // Click
                click: function () {
                    // Go to next page
                    this._goToPage(this.currentPageIndex + 1);
                }
            });
        },

        // Draw the carousel
        _drawCarousel: function () {
            this.items = this.carousel.find('.carousel-item');

            // Save the value of the list width now before it gets changed later on
            this.listWidth = this.list.width();
            // The page width is the simulated space that items can be in
            this.pageWidth = this.listWidth - this.prevArrow.outerWidth() - this.nextArrow.outerWidth();

            /*** Column/row calculations ***/
            // If the item width is larger than the page width, shrink it down
            var itemWidth = Math.min(this.options.itemWidth, this.pageWidth);

            // Set number of columns //
            if (this.options.multiCol == false) {
                // Single column
                this.nColumns = 1;
            } else {
                // Multiple columns
                this.nColumns = Math.min(Math.floor((this.pageWidth + this.options.itemMarginLeft) / (itemWidth + this.options.itemMarginLeft)), this.items.length);
            }
            // Set item width //
            if (this.options.growItemWidth == false) {
                // Fixed width
                this.itemWidth = itemWidth;
            } else {
                // Grow item width
                this.itemWidth = (this.pageWidth - (this.nColumns - 1) * this.options.itemMarginLeft) / this.nColumns;
            }
            // Set left margin (if there are multiple columns) //
            if (this.nColumns > 1) {
                if (this.options.growItemMarginLeft == false) {
                    // Fixed margin
                    this.itemMarginLeft = this.options.itemMarginLeft;
                } else {
                    // Grow margin
                    this.itemMarginLeft = (this.pageWidth - this.nColumns * this.itemWidth) / (this.nColumns - 1);
                }
            }

            // If the item height is larger than the page height, shrink it down
            // (This seems mostly redundant, doesn't it? The page height is always
            // enormously tall because the items go down in a column by default)
            var itemHeight = Math.min(this.options.itemHeight, this.list.height());

            // Set number of rows //
            if (this.options.multiRow == false) {
                // Single row
                this.nRows = 1;

                // Standardise list height to the height of the tallest item
                this.items.outerWidth(this.itemWidth);
                var largestHeight = -1;
                for (let i = 0; i < this.items.length; i++) {
                    largestHeight = Math.max(this.items.eq(i).outerHeight(), largestHeight);
                }

                var listHeight = Math.min(this.list.height() - this.nav.outerHeight(true), largestHeight);
                this.list.height(listHeight);
                itemHeight = listHeight;

                // Flex: 1 has served its purpose by now
                // Keeping it on FF and Edge causes the list to expand as tall as possible
                this.list.css('flex', 'none');
            } else {
                // Multiple rows
                this.nRows = Math.min(Math.floor((this.list.height() + this.options.itemMarginTop) / (itemHeight + this.options.itemMarginTop)), this.items.length);
            }
            // Set item height //
            if (this.options.growItemHeight == false) {
                // Fixed height
                this.itemHeight = itemHeight;
            } else {
                // Grow height
                this.itemHeight = (this.list.height() - (this.nRows - 1) * this.options.itemMarginTop) / this.nRows;
            }
            // Set top margin (if there are multiple rows) //
            if (this.nRows > 1) {
                if (this.options.growItemMarginTop == false) {
                    // Fixed margin
                    this.itemMarginTop = this.options.itemMarginTop;
                } else {
                    // Grow margin
                    this.itemMarginTop = (this.list.height() - this.nRows * this.itemHeight) / (this.nRows - 1);
                }
            }

            // Set the page count, which must be at least 1
            this.nPages = (this.items.length > 0) ? Math.ceil(this.items.length / (this.nColumns * this.nRows)) : 0;

            // Set the list width to 0, forcing the items to align themselves as left as possible
            // This works only because flex-direction: column is set on the list
            if (this.items.length > 0) {
                this.list.width(0);
            }

            /*** Draw carousel ***/
            // Set the item width
            this.items.outerWidth(this.itemWidth);
            // Set the left margin of all items except the left-most ones
            for (let c = 1; c < this.nColumns; c++) {
                for (let r = 0; r < this.nRows; r++) {
                    this.items.filter(':nth-child(' + this.nColumns * this.nRows + 'n + ' + (this.nRows * c + (r + 1)) + ')').css('margin-left', this.itemMarginLeft);
                }
            }
            // Distribute leftover horizontal space between the margins of the left and right-most items
            let leftoverX = this.listWidth - this.nColumns * this.itemWidth - (this.nColumns - 1) * this.itemMarginLeft;
            if (leftoverX > 0) {
                for (var i = 0; i < this.nRows; i++) {
                    this.items.filter(':nth-child(' + this.nColumns * this.nRows + 'n + ' + (i + 1) + ')').css('margin-left', leftoverX / 2);
                    this.items.filter(':nth-child(' + this.nColumns * this.nRows + 'n - ' + i + ')').css('margin-right', leftoverX / 2);
                }
            }

            // Set the item height
            this.items.outerHeight(this.itemHeight);
            // Set the top margin of all items except the top-most ones
            this.items.filter(':not(:nth-child(' + this.nRows + 'n + ' + 1 + '))').css('margin-top', this.itemMarginTop);
            // Distribute leftover vertical space between the margins of the top and bottom-most items
            var leftoverY = this.list.height() - this.nRows * this.itemHeight - (this.nRows - 1) * this.itemMarginTop;
            if (leftoverY > 0) {
                this.items.filter(':nth-child(' + this.nRows + 'n + ' + 1 + ')').css('margin-top', leftoverY / 2);
                this.items.filter(':nth-child(' + this.nRows + 'n)').css('margin-bottom', leftoverY / 2);
            }

            // Indicators
            this.nav.removeClass('nav-pagination').addClass('nav-abstract');
            this.pagination = this.options.pagination;

            var nPaginationIndicators = 9;
            var nIndicators;

            if (this.pagination) {
                nIndicators = nPaginationIndicators;
            } else {
                nIndicators = this.nPages;
            }

            this.progress.empty();
            for (let i = 0; i < nIndicators; i++) {
                this.progress.append('<button type="button" class="carousel-indicator"></button>');
            }

            if (nIndicators > 10) {
                this.pagination = true;
                nIndicators = nPaginationIndicators;

                if (this.progress.find('.carousel-indicator').length < 9) {
                    for (let i = this.progress.find('.carousel-indicator').length; i < nIndicators; i++) {
                        this.progress.append('<button type="button" class="carousel-indicator"></button>');
                    }
                } else if (this.progress.find('.carousel-indicator').length > 9) {
                    this.progress.find('.carousel-indicator').filter(function () {
                        return $(this).index() > nPaginationIndicators - 1;
                    }).remove();
                }
            }
            this.indicators = this.carousel.find('.carousel-indicator');

            if (this.pagination) {
                this.nav.removeClass('nav-abstract').addClass('nav-pagination');
            } else {
                this.nav.removeClass('nav-pagination').addClass('nav-abstract');
            }
        },

        _goToPage: function (pageIndex) {
            pageIndex = Math.max(0, Math.min(pageIndex, this.nPages - 1));

            this.list.css('transform', 'translateX(-' + pageIndex * this.listWidth + 'px)');
            this.currentPageIndex = pageIndex;

            this._checkControls();
        },

        _checkControls: function () {
            /** Check arrows **/
            if (this.currentPageIndex <= 0) {
                this.prevArrow.hide();
            } else {
                this.prevArrow.show();
            }
            if (this.currentPageIndex >= this.nPages - 1) {
                this.nextArrow.hide()
            } else {
                this.nextArrow.show();
            }

            /** Check indicators **/
            if (this.pagination == false) {
                // Abstract navigation //
                this.indicators.removeClass('prev current next');

                var currentInd = this.indicators.eq(this.currentPageIndex).addClass('current');
                currentInd.prev().addClass('prev');
                currentInd.next().addClass('next');
            } else {
                // Paginated navigation //
                this.indicators.empty();
                this.indicators.show();
                this.indicators.unbind();

                var indContent;
                var pageIndex = this.currentPageIndex - 4;

                for (let i = 0; i < this.indicators.length; i++) {
                    indContent = '';

                    if (pageIndex >= 0 && pageIndex <= this.nPages - 1) {
                        indContent = pageIndex + 1;

                        // First square
                        if (i == 0) {
                            indContent = 1;
                        }
                        // Second and penultimate sqaures
                        else if ((i == 1 && pageIndex > 1) || (i == this.indicators.length - 2 && pageIndex < this.nPages - 2)) {
                            indContent = '...';
                        }
                        // Last square
                        else if (i == this.indicators.length - 1) {
                            indContent = this.nPages;
                        }
                    }

                    this.indicators.eq(i).text(indContent);

                    if (this.indicators.eq(i).text() == '') {
                        this.indicators.eq(i).hide();
                    }

                    pageIndex++;
                }

                var currentInd = this.indicators.eq((this.indicators.length + 1) / 2 - 1).addClass('current');
                this.indicators.css('min-width', currentInd.height());
            }

            var location;

            // On indicator click, navigate to associated page
            for (let i = 0; i < this.indicators.length; i++) {
                this._on(this.indicators.eq(i), {
                    click: function () {
                        if (this.indicators.eq(i).text() == '...') {
                            if (this.indicators.eq(i).index() == 1) {
                                location = this.currentPageIndex - 5;
                            } else if (this.indicators.eq(i).index() == this.indicators.length - 2) {
                                location = this.currentPageIndex + 5;
                            }
                        } else {
                            if (this.pagination) {
                                location = this.indicators.eq(i).text() - 1;
                            } else {
                                location = i;
                            }
                        }
                        this._goToPage(location);
                    }
                });
            }
        },

        redraw: function (onResize = false) {
            var itemIndex;

            if (onResize) {
                // Set the item index
                itemIndex = this.nColumns * this.nRows * this.currentPageIndex;
            }

            // Reinitialise important values
            this.list.css('flex', '1');
            this.items.width('');
            this.items.height('');
            this.list.width('');
            this.list.height('');
            this.items.css('margin', '');

            // Redraw the carousel
            this._drawCarousel();

            // Correctly display the current page
            var pageIndex = (onResize && this.items.length > 0) ? Math.floor(itemIndex / (this.nRows * this.nColumns)) : this.currentPageIndex;
            this._goToPage(pageIndex);
        }
    });

})(jQuery, window, document);