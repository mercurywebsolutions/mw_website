; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.fadeAt", {
        options: {
            actionPosition: 0,
            speed: 800
        },

        _create: function () {
            this._on($(window), {
                scroll: function () {
                    // Hit the action position
                    if ($(window).scrollTop() >= this.options.actionPosition) {
                        // If fading out
                        if (this.element.hasClass('fading-out')) {
                            // Stop fading out
                            this.element.stop(true, false);
                            this.element.removeClass('fading-out');
                            // Pop back in
                            this.element.fadeIn(0);
                            return;
                        }

                        // If faded out and not fading in
                        if (this.element.css('display') == 'none' && this.element.hasClass('fading-in') == false) {
                            // Fade in
                            this.element.addClass('fading-in');
                            this.element.fadeIn(this.options.speed, function () {
                                $(this).removeClass('fading-in');
                            });
                            this.element.css('display', 'flex');
                        }
                    }
                    // Before the action position
                    else {
                        // If fading in
                        if (this.element.hasClass('fading-in')) {
                            // Stop fading in
                            this.element.stop(true, false);
                            this.element.removeClass('fading-in');
                            // Hide immediately
                            this.element.fadeOut(0);
                            return;
                        }

                        // If faded in and not fading out
                        if (this.element.css('display') != 'none' && this.element.hasClass('fading-out') == false) {
                            // Fade out
                            this.element.addClass('fading-out');
                            this.element.fadeOut(this.options.speed, function () {
                                $(this).removeClass('fading-out');
                            });
                        }
                    }
                }
            });
        }
    });
})(jQuery, window, document);