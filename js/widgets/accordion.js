; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.accordion", {
        options: {
        },

        _create: function () {
            this.panel = this.element.next();
            this.panelHeight = 0;

            // For each element inside the panel
            for (let i = 0; i < this.panel.children().length; i++) {
                // Add its height to panelHeight
                this.panelHeight += this.panel.children().eq(i).outerHeight(true);
            }

            // Set the panel height
            this.panel.css('--height', this.panelHeight);

            // On this
            this._on(this.element, {
                // click
                click: function () {
                    // Toggle accordion
                    this.toggleAccordion();
                }
            });
        },
        toggleAccordion: function () {
            this.element.toggleClass('open');
        }
    });
})(jQuery, window, document);