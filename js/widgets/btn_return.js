; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.btnReturn", {
        options: {
            triggerElement: $(window),
            speed: 800
        },

        _create: function () {
            // Set margin such that btn-return takes up no space in the DOM
            this.element.css('--height', this.element.outerHeight());

            // Scroll to top on click
            this.element.scrollTo({
                destination: $('html'),
                speed: this.options.speed
            });

            // Fade when going past the trigger element
            this.element.fadeAt({
                actionPosition: this.options.triggerElement.outerHeight(),
                speed: this.options.speed
            });

            // On window
            this._on($(window), {
                // Resize
                resize: function () {
                    if (this.element.data('mercuryFadeAt')) {
                        // Update trigger element height for FadeAt
                        this.element.fadeAt('option', 'actionPosition', this.options.triggerElement.outerHeight());
                    }
                },
                // Scroll
                scroll: function () {
                    // When btn-return hits the bottom of its parent
                    if ($(window).scrollTop() >= this.element.parent().next().offset().top - $(window).height()) {
                        // Position: absolute it
                        this.element.css('position', 'absolute');
                        this.element.css('margin-top', '0');
                    } else {
                        // Reset css
                        this.element.css('position', '');
                        this.element.css('margin-top', '');
                    }
                }
            });
        }
    });
})(jQuery, window, document);