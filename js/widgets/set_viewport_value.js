; (function ($, window, document, undefined) {

    'use strict';

    $.widget("mercury.setViewportValue", {
        options: {
            property: null,
            value: 100,
            minScreenHeight: 0
        },

        _create: function () {
            this._setValue();

            this._on($(window), {
                resize: function () {
                    this._setValue();
                }
            });
        },

        _setValue: function () {
            if ($(window).height() >= this.options.minScreenHeight) {
                // scrollHeight is the height of a mobile browser window WITHOUT the browser bar at the top
                this.element.css(this.options.property, this.options.value / 100 * $(window).height());

            } else {
                this.element.css(this.options.property, '');
            }
        },

        _destroy: function () {
        }
    });

})(jQuery, window, document);