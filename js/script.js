'use strict';

var $root = $(':root');
var $navbar = $('#navbar');

var scrollSpeed = 800;


$(document).ready(function () {
    // Set viewport values
    $root.setViewportValue({
        property: '--vh',
        value: 1
    });

    // Menu //
    // On click of 
    $('#btn-home').scrollTo({
        destination: $('html'),
        speed: scrollSpeed
    });

    // On btn-menu click
    $('#btn-menu').click(function () {
        // Toggle whether the menu is open
        $('#nav').toggleClass('open');
        $('body').toggleClass('nav-open');
    });

    // On menu button click
    $('.btn-section').click(function () {
        // Close the menu
        $('#nav').removeClass('open');
        $('body').removeClass('nav-open');
    }).each(function () {
        // Scroll to relevant section
        $(this).scrollTo({
            destination: $('#section-' + $(this).data('section')),
            navbar: $navbar,
            speed: scrollSpeed
        });
    });

    $('#navbar-contact').scrollTo({
        destination: $('#section-contact'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    // On enquire button click
    $('#btn-cta').scrollTo({
        destination: $('#section-pricing'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    // On return to top of section
    $('#btn-top').scrollTo({
        destination: $('#section-pricing'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    // Btn-return //
    $('#btn-return').btnReturn({
        speed: scrollSpeed
    });


    // Services //
    var currentAngle = 0;
    var servicesCount = $('.btn-service').length;
    var angle = 360 / servicesCount;

    // Initialise service button states
    $('#btn-design').addClass('active');
    $('#btn-build').addClass('prev');
    $('#btn-host').addClass('next');

    // Initialise service state
    $('#service-design').addClass('active');

    // Initialise feature states
    $('.service').each(function () {
        $(this).find('.btn-feature').first().addClass('active');
        $(this).find('.feature-desc').first().addClass('active');
    });

    // Override scrollTo for btn-services
    $.widget('mercury.serviceScrollTo', $.mercury.scrollTo, {
        // When clicking btn-service
        _scroll: function () {
            // If the active feature description is OUT OF VIEW
            if ($('.service.active .feature-desc.active').offset().top > $(window).scrollTop() + $(window).height()) {
                // Scroll to destination
                this._super();
            }
        }
    });

    // On click of btn-service
    $('.btn-service').click(function () {
        var direction = 0;

        // Determine the direction depending on which button was clicked
        // Set prev and next on the relevant buttons
        if ($(this).hasClass('prev')) {
            direction = 1;

            $('.btn-service.next').addClass('prev').removeClass('next');
            $('.btn-service.active').addClass('next');
        } else if ($(this).hasClass('next')) {
            direction = -1;

            $('.btn-service.prev').addClass('next').removeClass('prev');
            $('.btn-service.active').addClass('prev');
        } else {
            return;
        }

        // Make this the active button
        $('.btn-service.active').removeClass('active');
        $(this).addClass('active').removeClass('prev next');

        // Rotate the buttons and their parent
        var rotation = direction * angle;
        $('#services').css('transform', 'rotate(' + (currentAngle + rotation) + 'deg)');
        $('.btn-service').css('transform', 'rotate(' + (-currentAngle - rotation) + 'deg)');

        // Update the current angle
        currentAngle += rotation;

        // Display the relevant service content
        $('.service').removeClass('active');
        $('#service-' + $(this).data('service')).addClass('active');

        // Redraw the carousel
        $('.service.active .carousel').carousel('redraw');

    }).serviceScrollTo({
        destination: $('#section-services'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    $('.carousel').carousel({
        growItemWidth: true
    });

    /// Package Creator ///
    // Override the original accordion
    $.widget('mercury.pricingAccordion', $.mercury.accordion, {
        // When toggling the accordion
        toggleAccordion: function () {
            // If the element is not in a disabled category
            if (this.element.parent('.pricing-category').is('.disabled') == false) {
                // Call the original method
                this._super();
            }
        }
    });

    // Accordion any .btn-inputs that are accordions
    $('.btn-input.accordion').pricingAccordion();
    $('.btn-input').click(function (e) {
        /* On Firefox, swiping a btn-input will trigger the accordion but not the input */
        // Stops the input from being clicked automatically
        e.preventDefault();
        // Manually click the input
        $(this).find('.btn-input-input').click();
    });

    // On btn-input's input click
    $('.btn-input-input').click(function (e) {
        // Prevent the click from bubbling upwards
        // (stops the click on btn-input from registering twice, causing the accordion to open and close)
        e.stopPropagation();
    });

    // Enable the 'type' category
    SetCategoryState('type', true);

    // Set the inputs to which the 'type' buttons are attached
    $('.btn-input.secondary .btn-input-input[name=type]').eq(0).data('input', 'sections');
    $('.btn-input.secondary .btn-input-input[name=type]').eq(1).data('input', 'pages');
    // Hide the sections/pages inputs
    $('.btn-input-input[name=sections], .btn-input-input[name=pages]').parents('.btn-input').addClass('hidden');

    // On click of one of the 'type' buttons
    $('.btn-input.secondary .btn-input-input[name=type]').click(function () {
        var groupingName = $(this).data('input');
        var $input = $('.btn-input-input[name=' + groupingName + ']');
        var $selectedOption = $input.find('option:selected');

        // Enable the categories in step 2
        SetCategoryState('design', true);
        SetCategoryState('development', true);
        SetCategoryState('deployment', true);
        // If the associated input is hidden
        if ($input.parents('.btn-input').is('.hidden')) {
            // Hide both possible inputs
            $('.btn-input-input[name=sections], .btn-input-input[name=pages]').parents('.btn-input').addClass('hidden');
            // Show the associated input
            $input.parents('.btn-input').removeClass('hidden');
            // Update the 'design' and 'build' quote-items with the selected number of sections/pages
            $('#quote-item-design .quote-item-name').text('Design (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();
            $('#quote-item-development .quote-item-name').text('Build (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();
            $('#quote-item-copy .quote-item-name').text('Copywriting (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();
            // Update the 'design', 'build', and 'copy' prices to reflect the number of sections/pages
            var selectedVal = parseInt($selectedOption.text());
            var $inputDesign = $('.btn-input-input[name=design]');
            var $inputBuild = $('.btn-input-input[name=development]');
            $('#quote-item-design .quote-item-price').text('$' + parseInt(parseInt($inputDesign.val()) + selectedVal * $inputDesign.data(groupingName + '-price'))).flashAlert();
            $('#quote-item-development .quote-item-price').text('$' + parseInt(parseInt($inputBuild.val()) + selectedVal * $inputBuild.data(groupingName + '-price'))).flashAlert();
            $('#quote-item-copy .quote-item-price').text('$' + parseInt(selectedVal * $('.btn-input-input[name=copy]').data(groupingName + '-price'))).flashAlert();
        }
    });

    // Override scrollTo for pricing section
    $.widget('mercury.pricingScrollTo', $.mercury.scrollTo, {
        // When clicking btn-service
        _scroll: function () {
            // If the top of the pricing section is OUT OF VIEW
            if ($('#pricing-content').offset().top < $(window).scrollTop() + parseInt($(':root').css('--navbarHeight'))) {
                // Scroll to destination
                this._super();
            }
        }
    });

    // On btn-info click
    $('.btn-info').click(function (e) {
        // Prevent the click from bubbling upwards
        // (stops btn-input from opening when you click btn-info)
        e.stopPropagation();
        e.preventDefault();

        // Hide the pricing quote and any pricing-info windows
        $('#pricing-quote *').removeClass('flash')
        $('#pricing-quote, .pricing-info').hide();
        // Show the relevant pricing-info window
        $('#pricing-info-' + $(this).data('info')).show();
    }).pricingScrollTo({
        destination: $('#pricing-right'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    /// Quote template ///
    // Star the prices for these ites
    $('#quote-item-dnr, #quote-item-ssl').find('.quote-item-price').append('*');

    // On btn-input-input value change
    $('.btn-input-input').change(function () {
        var groupingName = $(this).attr('name');
        var $quoteItem = $('#quote-item-' + groupingName);
        var $quoteCategory = $('#quote-category-' + groupingName);

        // If the input is a checkbox or radio button
        if ($(this).is('[type=checkbox]') || $(this).is('[type=radio]')) {
            // Toggle the quote-item
            $quoteItem.toggle();

            /* flash animation when pricing button clicked */
            if ($(this).is(":checked")) {
                /* Need to first check if any feature inputs have already been selected, 
                if so animate them too */
                $(this).parent().siblings('.pricing-category-features').children().each(function () {
                    if ($(this).find('input[type = "checkbox"]').is(':checked')) {
                        var checkedFeature = $(this).find('input[type = "checkbox"]').attr('name');
                        var $checkedQuote = $('#quote-item-' + checkedFeature);
                        $checkedQuote.find('*').flashAlert();
                    }
                });
                $quoteItem.find('*').flashAlert();
            }

            // If the input is for a category
            if ($(this).parent('.btn-input').is('.primary')) {
                // Toggle the quote-category
                $quoteCategory.toggle();

                if (groupingName == 'deployment') {
                    $('#quote-conditions').toggle();

                    if ($(this).is(":checked")) {
                        $quoteCategory.find('#quote-item-hosting').find('*').flashAlert();
                    }
                }
            }
            // If the input is a dropdown
        } else if ($(this).is('select')) {
            var $selectedOption = $(this).find('option:selected');
            // Hide all quote-options for the dropdown
            $quoteItem.find('.quote-item-option').hide();
            // Show the newly active quote-option for the dropdown
            $quoteItem.find('.quote-item-option').eq($selectedOption.index()).show().flashAlert();

            // If this is a 'sections' or 'pages' dropdown
            if ($(this).is('[name=sections], [name=pages]')) {
                // Update the 'design' and 'build' quote-items with the selected number of sections/pages
                $('#quote-item-design .quote-item-name').text('Design (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();
                $('#quote-item-development .quote-item-name').text('Build (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();
                $('#quote-item-copy .quote-item-name').text('Copywriting (' + $selectedOption.text() + ' ' + groupingName + ')').flashAlert();

                // Update the 'design' and 'build' prices to reflect the number of sections/pages
                var selectedVal = parseInt($selectedOption.text());
                var $inputDesign = $('.btn-input-input[name=design]');
                var $inputBuild = $('.btn-input-input[name=development]');
                $('#quote-item-design .quote-item-price').text('$' + parseInt(parseInt($inputDesign.val()) + selectedVal * $inputDesign.data(groupingName + '-price'))).flashAlert();
                $('#quote-item-development .quote-item-price').text('$' + parseInt(parseInt($inputBuild.val()) + selectedVal * $inputBuild.data(groupingName + '-price'))).flashAlert();
                $('#quote-item-copy .quote-item-price').text('$' + parseInt(selectedVal * $('.btn-input-input[name=copy]').data(groupingName + '-price'))).flashAlert();
            }
        }

        // Add up the quote numbers
        SumQuote();
    });

    // 'Check' any permanent items
    $('.permanent').click()
        // Prevent them from ever being unchecked
        .off('click').click(function (e) {
            e.preventDefault();
        });

    // Hide any pricing-info windows
    $('.btn-back, .btn-estimate, .btn-input').click(function () {
        $('.pricing-info').hide();
        // Show the quote
        $('#pricing-quote').show();
        // Add up the quote numbers (necessary because the quote was hidden)
        SumQuote();
    });

    $('.btn-estimate').scrollTo({
        destination: $('#pricing-right'),
        navbar: $navbar,
        speed: scrollSpeed
    });

    // Initially and on resize
    OnResize();

    /// On resize of window ///
    $(window).resize(function () {
        // Turn off transitions
        $('*').toggleClass('notransition');
        // Perform onResize functions
        OnResize();
        // Turn on transitions
        $('*').toggleClass('notransition');
    });

    /// On scroll of window ///
    $(window).scroll(function () {
    });

    // Get a list of all gifs
    var gif = getGif();

    // Preloads gifs by creating new image objects and applying the 
    //  source of the gif to each one
    var image = [];
    $.each(gif, function (index) {
        image[index] = new Image();
        image[index].src = gif[index];
    });

    /// On click of gif play/stop ///
    $('.gif-container.gif').on('click', function () {
        // Store link to gif(gifSrc) & img source (gifAlt)
        var $this = $(this),
            $gif = $this.children('img'),
            $gifSrc = $gif.attr('src'),
            $gifAlt = $gif.attr('data-alt'),
            $gifExt = $gifAlt.split('.');
        // Toggle between gif & still image by checking file extension
        if ($gifExt[1] === 'gif') {
            $gif.attr('src', $gif.data('alt')).attr('data-alt', $gifSrc);
        } else {
            $gif.attr('src', $gifAlt).attr('data-alt', $gif.data('alt'));
        }
        $this.toggleClass('play-gif');
        $this.children('i').toggleClass('play-gif');
    });

    // Contact form //
    var $submit = $('#form-contact input[type=submit]');
    var $response = $('#form-contact-response');

    $('#form-contact').submit(function (e) {
        e.preventDefault();

        $submit.val('Sending...');

        $.ajax({
            url: 'php/send_enquiry.php',
            type: 'post',
            data: $(this).serialize(),
            success: function (data) {
                // Parse return data
                data = JSON.parse(data);

                // Fade out response text
                $response.fadeTo('fast', 0, function () {
                    // Set and fade in response text
                    $response.removeClass('error').html(data.msg).fadeTo('fast', 100);

                    // Handle error or success
                    if (data.success == false) {
                        $response.addClass('error');
                        $submit.val('Send');
                    } else {
                        $submit.prop('disabled', true).val('Sent!');
                    }
                });
            },
            error: function (data) {
                // Fade out response text
                $response.fadeTo('fast', 0, function () {
                    // Display error
                    $submit.val('Send');
                    $response.html('Oops! Something went wrong and we couldn\'t send your message.<br>Please try again.');
                    $response.addClass('error').fadeTo('fast', 100);
                });
            }
        });
    });
});

// Initially and on resize
function OnResize() {
    // If not on a phone
    if ($(window).width() >= 768 && $(window).height() > 500) {
        // Close menu
        $('#nav').removeClass('open');
        $('body').removeClass('nav-open');
    }

    var $btnService = $('.btn-service').first();
    var offset = $btnService.outerHeight() - $btnService.find('.service-title').outerHeight(true);

    // Set section-services offset
    $root.css('--serviceIconHeight', offset);

    // Header background image
    var $bgImg = $('#header-background_image');
    var $bgColor = $('#header-background_color');
    var bgAngle = parseInt($bgColor.css('--angle'));
    var bgHeight = $bgImg.height() / 2;
    var headerWidth = $bgColor.width();
    var extraWidth = bgHeight / Math.tan((180 - bgAngle) * Math.PI / 180);
    $bgImg.css('--bgWidth', (headerWidth / 2) + extraWidth + 'px');

    // Process background
    var $processBg = $('#process-background');
    var processBgAngle = parseInt($processBg.css('--angle')) * -1 * Math.PI / 180;
    var processBgWidth = parseFloat($processBg.css('--width')) * $(window).width();

    $processBg.outerWidth(processBgWidth / Math.cos(processBgAngle));
    $processBg.outerHeight(processBgWidth * Math.sin(processBgAngle));

    // If there's not enough space to see the info window
    if ($(window).width() < 1024 && $(window).height() < 1024) {
        if ($('.btn-info').data('mercuryScrollTo') == undefined) {
            // Add scroll-to btn-info
            $('.btn-info').scrollTo({
                destination: $('#pricing-right'),
                navbar: $navbar,
                speed: scrollSpeed
            });
        }
    } else {
        if ($('.btn-info').data('mercuryScrollTo') != undefined) {
            // Remove scroll-to from btn-info
            $('.btn-info').scrollTo('destroy');
        }
    }
}

// Toggle the state of a pricing-category
function SetCategoryState(categoryName, state) {
    var $category = $('#pricing-category-' + categoryName);
    state == true ? $category.removeClass('disabled') : $category.addClass('disabled');
    $category.find('.btn-input.primary .btn-input-input').prop('disabled', !state);
}

// Sum up the subtotals and total of the quote
function SumQuote() {
    var total = 0;
    var $total = $('#quote-total');

    // For each visible quote-category
    $('.quote-category:visible').each(function () {
        var subtotal = 0;

        // For each visible price
        $(this).find('.quote-item-price:visible').each(function () {
            // Add this to the category subtotal
            if ($(this).text().includes("FREE") == false) {
                subtotal += parseInt($(this).text().replace('$', ''));
            }
        });

        // Update the subtotal
        $(this).find('.quote-subtotal').text('Subtotal: $' + subtotal);
        // Add the subtotal to the total
        total += subtotal;
    });

    // Update the total
    $total.text('Total: $' + total);
    total > 0 ? $total.show() : $total.hide();
}

function getGif() {
    var gif = [];
    $('.info-gif').each(function () {
        var data = $(this).data('alt');
        if (data != "") {
            gif.push(data);
        }
    });
    return gif;
}

/* A chain function for the flash animation */
$.fn.flashAlert = function () {
    var $this = $(this);
    $(this).removeClass('flash');
    setTimeout(function () {
        $this.addClass("flash");
    }, 0);
    return this;
}