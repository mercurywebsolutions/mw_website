<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137548613-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-137548613-1');
    </script>

    <link rel="icon" type="image/png" href="images/favicon.png">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Mercury Web Solutions | Web Design Melbourne</title>
    <meta name="description" content="Mercury Web Solutions designs, builds and hosts websites for businesses in Melbourne. Each of our clients get a website tailored specifically to their business." />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:400,400i,500,600,700" rel="stylesheet" />
    <link rel="stylesheet" href="css/initial.css" />
    <link rel="stylesheet" href="css/widgets/accordion.css" />
    <link rel="stylesheet" href="css/widgets/btn-return.css" />
    <link rel="stylesheet" href="css/widgets/carousel.css" />
    <link rel="stylesheet" href="css/theme/style.css" />
    <link rel="stylesheet" href="css/theme/body.css" />
    <link rel="stylesheet" href="css/theme/fonts.css" />
    <link rel="stylesheet" href="css/theme/animate.css" />
    <link rel="stylesheet" href="css/theme/sections/header.css" />
    <link rel="stylesheet" href="css/theme/sections/services.css" />
    <link rel="stylesheet" href="css/theme/sections/about.css" />
    <link rel="stylesheet" href="css/theme/sections/process.css" />
    <link rel="stylesheet" href="css/theme/sections/pricing.css" />
    <link rel="stylesheet" href="css/theme/sections/contact.css" />
    <link rel="stylesheet" href="css/theme/sections/footer.css" />
    <link rel="stylesheet" href="css/theme/z-indexes.css" />
</head>

<body>
    <div id="navbar">
        <button id="btn-home" type="button">
            <img id="navbar-logo" src="images/logo_mobile.png" alt="Logo">
        </button>
        <nav id="nav">
            <ul id="nav-list">
                <li>
                    <button class="btn-section" type="button" data-section="services">Services</button>
                </li>
                <li>
                    <button class="btn-section" type="button" data-section="team">Team</button>
                </li>
                <li>
                    <button class="btn-section" type="button" data-section="process">Process</button>
                </li>
                <!--<li>
                    <button class="btn-section" type="button" data-section="pricing">Estimate</button>
                </li>-->
                <li>
                    <button class="btn-section" type="button" data-section="contact">Enquire</button>
                </li>
            </ul>
        </nav>
        <button id="navbar-contact" type="button">
            <i id="contact-icon" class="fas fa-phone-square"></i>
            <p id="contact-number" class="hide-mobile hide-tablet">GET A QUOTE</p>
        </button>
        <button id="btn-menu" type="button">
            <i class="material-icons">menu</i>
        </button>
    </div>
    <header id="header">
        <div id="header-background_image"></div>
        <div id="header-background_color"></div>
        <div id="header-content">
            <h1 class="hidden">Mercury Web Solutions</h1>
            <p id="header-location" class="emphasis">Melbourne&nbsp;AUS</p>
            <img id="logo" src="images\logo.png" alt="Mercury Web Solutions">
            <p id="desc" class="emphasis">
                Websites built from scratch, just for you
            </p>
            <!--<button id="btn-cta" class="btn" type="button">Get an estimate</button>-->
        </div>
    </header>
    <main>
        <section id="section-services" class="section-alt">
            <h2 class="hidden">Our Services</h2>
            <?php include "php/populate_services.php"; ?>
        </section>
        <section id="section-team">
            <h2>Our team</h2>
            <div id="team-img">
                <!-- <p id="name-james" class="team-name">James</p>
                <p id="name-cam" class="team-name">Cameron</p> -->
            </div>
            <p id="team-desc" class="text container">Cameron and James are both the designers and programmers of Mercury Web Solutions, having studied and worked together for many years. Their combined experience of 8 years in the webisphere has given them the ability to tackle any scenario. They founded Mercury Web with the aim of providing businesses with high-quality, personalised websites. Cameron is based in Melbourne and James works out of the Mornington Peninsula.</p>
        </section>
        <section id="section-process" class="section-alt">
            <h2>Our process</h2>
            <div id="process" class="container">
                <?php include "php/populate_process.php"; ?>
            </div>
            <div id="process-background"></div>
        </section>
        <!--<section id="section-pricing" class="container">
            <h2>Pricing estimate</h2>
            <p>Choose the features you desire for your website to receive a <i>rough estimate</i> of what it will cost to build. Click on the <span style="color: #66bdff; font-weight: 700">info</span> icons to see more information.</p>
            <div id="pricing-content">
                <form id="form-pricing" autocomplete="off">
                    <?php //include "php/populate_pricing_steps.php"; ?>
                    <button class="btn btn-estimate" type="button">To estimate</button>
                </form>
                <div id="pricing-right">
                    <div id="pricing-quote">
                        <p class="pricing-title emphasis">Get your estimate</p>
                        <?php //include "php/populate_pricing_quote.php"; ?>
                    </div>
                    <?php //include "php/populate_pricing_info.php"; ?>
                </div>
            </div>
            <button class="btn btn-estimate hide-tablet hide-desktop" type="button">To estimate</button>
            <button id="btn-top" type="button">&mdash; Back to top &mdash;</button>
            <p id="pricing-cta">For a more accurate quote, see the section below for ways to contact us!</p>
        </section>-->
        <section id="section-contact">
            <div id="contact-container" class="container">
                <h2>Enquire now</h2>
                <p>Get in touch to tell us about your website needs! Alternatively, feel free to ask us any questions you may have!</p>
                <address id="contact-info">
                    <div class="contact-info-row">
                        <div class="contact-info-item">
                            <i class="contact-info-item-icon fas fa-envelope"></i>
                            <a class="contact-info-item-detail" href="mailto:hello@mercuryweb.com.au">hello@mercuryweb.com.au</a>
                        </div>
                        <div class="contact-info-item">
                            <i class="contact-info-item-icon fas fa-phone"></i>
                            <a class="contact-info-item-detail" href="tel:0450245867">0425 533 447</a>
                        </div>
                    </div>
                    <div class="contact-info-row">
                        <div class="contact-info-item">
                            <i class="contact-info-item-icon fab fa-facebook-f"></i>
                            <a class="contact-info-item-detail" href="https://wwww.facebook.com/mercurywebsolutions/" target="_blank">/mercurywebsolutions</a>
                        </div>
                        <div class="contact-info-item">
                            <i class="contact-info-item-icon fas fa-map-marker-alt"></i>
                            <p class="contact-info-item-detail">Melbourne, Australia</p>
                        </div>
                    </div>
                </address>
                <form id="form-contact" autocomplete="off">
                    <div class="form-row">
                        <input type="text" class="form-field" name="name" placeholder="Name" required>
                        <input type="text" class="form-field" name="business" placeholder="Business name"></div>
                    <div class="form-row">
                        <input type="tel" class="form-field" name="phone" placeholder="Contact number">
                        <input type="email" class="form-field" name="email" placeholder="Email" required>
                    </div>
                    <input type="text" class="form-field" name="subject" placeholder="Subject">
                    <textarea class="form-field" name="message" rows="5" placeholder="Your message" required></textarea>
                    <input class="form-field btn" type="submit" value="Send">
                </form>
                <p id="form-contact-response"></p>
            </div>
        </section>

        <button id="btn-return" type="button">
            <i class="material-icons">keyboard_arrow_up</i>
        </button>
    </main>
    <footer id="footer">
        <div class="footer-seperator"></div>
        <p><a href="docs/Privacy_Policy.pdf" target="_blank">Privacy Policy</a><br>Copyright &copy; Mercury Web Solutions 2019</p>
    </footer>
    <!-- #region Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/widgets/accordion.js"></script>
    <script src="js/widgets/btn_return.js"></script>
    <script src="js/widgets/carousel.js"></script>
    <script src="js/widgets/jquery.hammer.js"></script>
    <script src="js/widgets/fade_at.js"></script>
    <script src="js/widgets/scroll_to.js"></script>
    <script src="js/widgets/set_viewport_value.js"></script>
    <!-- #endregion -->
</body>

</html>