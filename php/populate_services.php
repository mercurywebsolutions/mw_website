<div id="services">
    <?php
        $services = json_decode(file_get_contents("json/services.json"), true);
        foreach ($services as $service) :
    ?>
    <button id="btn-<?php echo $service["name"]; ?>" class="btn-service" type="button" data-service="<?php echo $service["name"]; ?>">
        <i class="service-icon material-icons"><?php echo $service["icon"]; ?></i>
        <p class="service-title">We <?php echo $service["name"]; ?></p>
    </button>
    <?php
        endforeach;
    ?>
</div>
<?php
    foreach ($services as $service) :
?>
<div id="service-<?php echo $service["name"]; ?>" class="service container">
    <span class="cosmetic">Our <?php echo $service["product"]; ?></span>
    <div class="service-carousel carousel">
        <div class="carousel-list">
        <?php
            foreach ($service["features"] as $feature) :
        ?>
        <div class="carousel-item feature">
            <p class="feature-name emphasis"><?php echo $feature["name"]; ?></p>
            <p id="feature-desc-<?php echo $feature["id"]; ?>" class="feature-desc text"><?php echo $feature["desc"]; ?></p>
        </div>
        <?php
            endforeach;
        ?>
        </div>
        <button class="carousel-arrow prev" type="button">
            <i class="material-icons">chevron_left</i>
        </button>
        <button class="carousel-arrow next" type="button">
            <i class="material-icons">chevron_right</i>
        </button>
        <div class="carousel-nav nav-abstract">
            <div class="carousel-progress">
                <button type="button" class="carousel-indicator current"></button>
            </div>
        </div>
    </div>
</div>
<?php
    endforeach;
?>