<?php
    $process = json_decode(file_get_contents("json/processes.json"), true);
    $count = 0;

    foreach ($process as $step) :
        $direction = ($count % 2 == 0 ? " left" : "right" )
?>
<div class="process-step <?php echo $direction; ?>">
    <i class="process-step-icon material-icons"><?php echo $step["icon"]; ?></i>
    <div class="process-step-info">
        <p class="process-step-desc">
            <span class="process-step-name"><?php echo $step["name"]; ?></span>.
            <?php echo $step["desc"]; ?>
        </p>
    </div>
</div>
<?php
    $count++;
    endforeach;
?>