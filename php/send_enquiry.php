<?php
    // Set return variables
    $success = false;
    $msg = "";
    $return = [
        "success" => false,
        "msg" => "Thank you! Your message has been sent!"
    ];

    // Retrieve and sanitise input
    $name = sanitise($_POST["name"]);
    $business = sanitise($_POST["business"]);
    $phone = sanitise($_POST["phone"]);
    $subject = sanitise($_POST["subject"]);
    $email = sanitise($_POST["email"]);
    $message = sanitise($_POST["message"]);

    // Check to see if any fields are empty
    if (empty($name) || empty($email) || empty($message)) {
        $return["msg"] = "Please fill out all required fields and try again";
        echo json_encode($return);
        return;
    }

    // Validate email address
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $return["msg"] = "Please enter a valid email address and try again";
        echo json_encode($return);
        return;
    }

    // Set email paramaters
    $to = "hello@mercuryweb.com.au";
    $msgSubject = $subject == "" ? "Contact form enquiry" : $subject;
    $body = "Hi! You have received an enquiry from " . $name . "\nBusiness: " . $business . "\nPhone number: " . $phone . "\n\n" . wordwrap($message, 70);
    $headers = "From: " . $email;

    // Try to send email
    if (!mail($to, $msgSubject, $body, $headers)) {
        // If it fails, return an Internal Server Error
        http_response_code(500);
        return;
    }

    // If it succeeds, return a success message
    $return["success"] = true;
    echo json_encode($return);

    // Sanitisation function
    function sanitise($input) {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
?>