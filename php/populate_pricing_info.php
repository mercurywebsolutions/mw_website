<?php
    $steps = json_decode(file_get_contents("json/pricing.json"), true);
    foreach ($steps as $step) :
        foreach ($step["categories"] as $category) :
?>
<div id="pricing-info-<?php echo $category["info-tag"]; ?>" class="pricing-info text">
    <button class="btn-back" type="button"><i class="fas fa-times"></i></button>
    <p class="pricing-title emphasis">Info: <?php echo $category["name"]; ?></p>
    <?php echo $category["info"]; ?>
    <?php
    $dir = "images/info";
    $src = $dir . "/".$category["id"];
    $imgFile = glob($src.".{png,PNG}" , GLOB_BRACE);
    if (!empty($imgFile))
    {
        $gif = "";
        $type = "img";
        $gifFile = glob($src.".{gif,GIF}" , GLOB_BRACE);
        if (!empty($gifFile))
        {
            $gif = $gifFile[0];
            $type = "gif";
        }
    ?> 
    <!-- Replace string with variables related to pricing.json -->
    <div class="gif-container <?php echo $type;?>">
        <i class="fas fa-play"></i>
        <img id="info-gif-<?php echo $category["id"]; ?>" class="info-gif" src="<?php echo $imgFile[0]; ?>" data-alt="<?php echo $gif; ?>"  alt="info-<?php echo $category["id"]; ?>">
    </div>
    <?php
    }
    ?>
</div>
        <?php
            foreach ($category["features"] as $feature) :
        ?>
<div id="pricing-info-<?php echo $feature["info-tag"]; ?>" class="pricing-info text">
    <button class="btn-back" type="button"><i class="fas fa-times"></i></button>
    <p class="pricing-title emphasis">Info: <?php echo $feature["name"]; ?></p>
    <?php echo $feature["info"]; ?>
    <?php
    $dir = "images/info";
    $src = $dir . "/".$feature["id"];
    $imgFile = glob($src.".{png,PNG}" , GLOB_BRACE);
    if (!empty($imgFile))
    {
        $gif = "";
        $type = "img";
        $gifFile = glob($src.".{gif,GIF}" , GLOB_BRACE);
        if(!empty($gifFile))
        {
            $gif = $gifFile[0];
            $type = "gif";
        }
    ?>
    <!-- Replace string with variables related to pricing.json -->
    <div class="gif-container <?php echo $type;?>">
        <i class="fas fa-play"></i>
        <img id="info-gif-<?php echo $feature["id"]; ?>" class="info-gif" src="<?php echo $imgFile[0]; ?>" data-alt="<?php echo $gif; ?>" alt="info-<?php echo $feature["id"]; ?>">
    </div>
    <?php
    }
    ?>
</div>
<?php
            endforeach;
        endforeach;
    endforeach;
?>