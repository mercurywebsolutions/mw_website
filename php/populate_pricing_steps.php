<?php
    function getProperty($item, $prop) {
        return isset($item[$prop]) ? $item[$prop] : "";
    }

    $steps = json_decode(file_get_contents("json/pricing.json"), true);
    $i = 1;
    foreach ($steps as $step) :
?>
<div class="pricing-step">
    <p><span class="emphasis"><?php echo $i; ?>.</span> <?php echo $step["text"]; ?></p>
    <?php
        foreach ($step["categories"] as $category) :
            $id = getProperty($category, "id");
            $permanent = getProperty($category, "permanent");
            $name = getProperty($category, "name");
            $type = getProperty($category, "type");
            $price = getProperty($category, "price");
            $sectionPrice = getProperty($category, "section-price");
            $pagePrice = getProperty($category, "page-price");
            $infoTag = getProperty($category, "info-tag");
            $info = getProperty($category, "info");
    ?>
    <div id="pricing-category-<?php echo $id; ?>" class="pricing-category disabled">
        <label class="btn-input primary accordion <?php if ($permanent) echo "permanent"; ?>">
            <input class="btn-input-input" type="<?php echo $type; ?>" name="<?php echo $id; ?>" value="<?php echo $price; ?>" data-sections-price="<?php echo $sectionPrice; ?>" data-pages-price="<?php echo $pagePrice; ?>" disabled>
            <div class="btn-input-indicator <?php echo $type; ?> material-icons"></div>
            <p class="btn-input-name"><?php echo $name; ?></p>
            <button class="btn-info" type="button" data-info="<?php echo $infoTag; ?>">
                <i class="material-icons">info</i>
                <div class="info-tooltip">Read more</div>
            </button>
        </label>
        <div class="pricing-category-features">
            <?php
                foreach ($category["features"] as $feature) :
                    $id = getProperty($feature, "id");
                    $permanent = getProperty($feature, "permanent");
                    $name = getProperty($feature, "name");
                    $type = getProperty($feature, "type");
                    $price = getProperty($feature, "price");
                    $sectionPrice = getProperty($feature, "section-price");
                    $pagePrice = getProperty($feature, "page-price");
                    $infoTag = getProperty($feature, "info-tag");
                    $info = getProperty($feature, "info");
            ?>
            <label class="btn-input secondary <?php if ($permanent) echo "permanent"; ?>">
                <?php if ($type == "select") : ?>
                <div class="custom-select">
                    <select class="btn-input-input" name="<?php echo $id; ?>">
                        <?php foreach ($feature["options"] as $option) : ?>
                        <option value="<?php echo $option["price"]; ?>"><?php echo $option["name"]; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php else: ?>
                <input class="btn-input-input" type="<?php echo $type; ?>" name="<?php echo $id; ?>" value="<?php echo $price; ?>" data-sections-price="<?php echo $sectionPrice; ?>" data-pages-price="<?php echo $pagePrice; ?>">
                <?php endif; ?>
                <div class="btn-input-indicator <?php echo $type; ?> material-icons"></div>
                <p class="btn-input-name"><?php echo $name; ?></p>
                <button class="btn-info" type="button" data-info="<?php echo $infoTag; ?>">
                    <i class="material-icons">info</i>
                    <div class="info-tooltip">Read more</div>
                </button>
            </label>
            <?php
                $count++;
                endforeach;
            ?>
        </div>
    </div>
    <?php
        endforeach;
    ?>
</div>
<?php
    $i++;
    endforeach;
?>