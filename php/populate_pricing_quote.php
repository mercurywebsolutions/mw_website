<?php
    $steps = json_decode(file_get_contents("json/pricing.json"), true);
    foreach ($steps as $step) :
        if($step["id"] == "services") :
        foreach ($step["categories"] as $category) :
?>
<div id="quote-category-<?php echo $category["id"]; ?>" class="quote-category">
    <div id="quote-item-<?php echo $category["id"]; ?>" class="quote-item primary">
        <div class="quote-item-details">
            <p class="quote-item-name"><?php echo $category["name"]; ?></p>
            <p class="quote-item-price"><?php echo $category["price"] == 0 ? "FREE" : "$" . $category["price"]; ?></p>
        </div>
        <p class="quote-item-desc"><?php echo $category["description"]; ?></p>
    </div>
    <?php
        foreach ($category["features"] as $feature) :
    ?>
    <div id="quote-item-<?php echo $feature["id"]; ?>" class="quote-item secondary">
        <?php
            if ($feature["type"] == "select") :
                foreach ($feature["options"] as $option) :
        ?>
        <div class="quote-item-option">
            <div class="quote-item-details">
                <p class="quote-item-name"><?php echo $option["name"] . " " . $feature["name"]; ?></p>
                <p class="quote-item-price">$<?php echo $option["price"]; ?></p>
            </div>
            <p class="quote-item-desc"><?php echo $option["description"]; ?></p>
        </div>
        <?php
                endforeach;
            else:
        ?>
        <div class="quote-item-details">
            <p class="quote-item-name"><?php echo $feature["name"]; ?></p>
            <p class="quote-item-price"><?php echo $feature["price"] == 0 ? "FREE" : "$" . $feature["price"]; ?></p>
        </div>
        <p class="quote-item-desc"><?php echo $feature["description"]; ?></p>
        <?php endif; ?>
    </div>
    <?php endforeach; ?>
    <p class="quote-subtotal"></p>
</div>
<?php
        endforeach;
    endif; endforeach;
?>
<p id="quote-conditions">*No extra fees for set-up or administration. Cost of product still applies.</p>
<p id="quote-total"></p>